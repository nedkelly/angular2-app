import { Component } from "@angular/core";

@Component({
  selector: "app",
  template: "<h1>Angular2 Start App - Hello World!</h1>"
})

export class AppComponent { }
