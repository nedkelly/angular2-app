
import chalk from "chalk";
import gutil from "gulp-util";

/**
 * Whenever any plugin throws an error, this function will handle it.
 * @param  {Object} err
 * @return {void}
 */
module.exports.errorHandler = (err) => {
  gutil.log(chalk.red(`Whoops, something has gone horribly wrong!\n\n${chalk.yellow(err)}\n\n`));
};
